package com.company.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"Data",
"FileName",
"FilePath",
"ID",
"IsFile"
})
@Entity
@Table(name="pressRelease")
public class PressRelease {
	
	@JsonProperty("ID")
	@Id
	private long id;
	
	@JsonProperty("IsFile")
	private boolean isFile;
	
	@JsonProperty("FileName")
	private String fileName;
	
	
	@JsonProperty("FilePath")
	private String filePath;
	
	@JsonProperty("Data")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "pressRel_id")
	private Set<Data> data = new HashSet<Data>();
	
	public PressRelease() {
		
	}
	
//	public PressRelease(long id, boolean isFile, String fileName, String filePath, Set<Data> data) {
//		super();
//		this.id = id;
//		this.isFile = isFile;
//		this.fileName = fileName;
//		this.filePath = filePath;
//		this.data = data;
//	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public boolean getIsFile() {
		return isFile;
	}
	
	public void setIsFile(boolean isFile) {
		this.isFile = isFile;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	public Set<Data> getData() {
		return data;
	}

	public void setData(Set<Data> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "PressRelease [id=" + id + ", isFile=" + isFile + ", fileName=" + fileName + ", filePath=" + filePath
				+ ", data=" + data + "]";
	}

}
