package com.company.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"Key",
"Value"
})
@Entity
@Table(name="data")
public class Data {
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@JsonProperty("Key")
	private String key;
	
	@JsonProperty("Value")
	@Lob
	private String value;
	
//	@JsonIgnore
//	@ManyToOne(targetEntity = PressRelease.class,fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//	@JoinColumn(name = "presRel_id", referencedColumnName = "id")
//    private PressRelease pressRelease;
	
	public Data() {
		
	}
	
//	public Data(String key, String value, PressRelease pressRelease) {
//		super();
//		this.key = key;
//		this.value = value;
//		this.pressRelease = pressRelease;
//	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

//	public PressRelease getPressRelease() {
//		return pressRelease;
//	}
//
//	public void setPressRelease(PressRelease pressRelease) {
//		this.pressRelease = pressRelease;
//	}

	@Override
	public String toString() {
		return "Data [id=" + id + ", key=" + key + ", value=" + value + "]";
	}

}
