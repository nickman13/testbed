package com.company.utils;

import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.FactoryBean;

public class CustomHttpClientFactory implements FactoryBean<HttpClient> {

    @Override
    public HttpClient getObject() throws Exception {
    	TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
	    HostnameVerifier allPassVerifier = (String s, SSLSession sslSession) -> true;  // ignore hostnaem checking
	 
	    SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
	        .loadTrustMaterial(null, acceptingTrustStrategy).build(); // keystore is null, not keystore is used at all
	 
	    SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, allPassVerifier);
	    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
        return httpClient;
    }

    @Override
    public Class<HttpClient> getObjectType() {
        return HttpClient.class;
    }
    
    @Override
    public boolean isSingleton() {
        return true;
    }

}