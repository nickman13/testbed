package com.company.scheduler;

import org.zkoss.bind.annotation.Command;

import static org.zkoss.zk.ui.util.Clients.showNotification;

import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

public class BoardViewModel {
	
	private static Scheduler scheduler;

	public void setScheduler(Scheduler scheduler) {
		BoardViewModel.scheduler = scheduler;
	}

	@Command
    public void startGetNewsJob() {
		try {
			scheduler.triggerJob(new JobKey("epant_get_news"));
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        showNotification("I am the server, you just poked me!", true);
    }
}
