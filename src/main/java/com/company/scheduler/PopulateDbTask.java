package com.company.scheduler;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.company.jpa.PressReleaseService;
import com.company.model.PressRelease;

@Service
public class PopulateDbTask {
	
	private final static Logger logger = Logger.getLogger(PopulateDbTask.class.getName());
	
	private final String url = "https://www.epant.gr/openData.svc/GetNews";
	
	@Autowired
	PressReleaseService service;
	
	private RestTemplate restTemplate;
	
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	public void populateDB() {
		logger.log(Level.INFO, "Population started at "+ Calendar.getInstance().getTime());
//		System.out.println("Database population started at "+ Calendar.getInstance().getTime());
		PressRelease[] pressReleases = restTemplate.getForObject(url, PressRelease[].class);
		List<PressRelease> pressRelList = Arrays.asList(pressReleases);
		
		pressRelList.forEach(item->service.save(item));
	}

}