package com.company.scheduler;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class JobScheduler extends QuartzJobBean {
	
	private PopulateDbTask populateDbTask;

	
	public void setPopulateDbTask(PopulateDbTask populateDbTask) {
		this.populateDbTask = populateDbTask;
	}

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
			populateDbTask.populateDB();
	}
	
	
}