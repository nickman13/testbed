package com.company.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.company.model.PressRelease;

public interface PressReleaseJpaRepository extends JpaRepository<PressRelease, Long> {

}