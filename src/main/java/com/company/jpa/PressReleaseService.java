package com.company.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

import com.company.model.PressRelease;

@Service
public class PressReleaseService {
	
	@Autowired
	PressReleaseJpaRepository pressReleaseRepository;
 
	public void save(PressRelease pressRelease){
		pressReleaseRepository.save(pressRelease);
	}
	
	@Transactional
    public List<PressRelease> findAll() {
        return pressReleaseRepository.findAll();
    }
	
	@Transactional
	public void showData(){
		System.out.println("=====================Retrieve PressRelease from Database:====================");
		List<PressRelease> pressLst = pressReleaseRepository.findAll();
		System.out.println("=====================Show All PressReleases on console:====================");
		pressLst.forEach(System.out::println);;
	}
}
