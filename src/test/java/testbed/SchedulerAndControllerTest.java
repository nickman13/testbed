package testbed;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration("classpath:spring-config.xml")
@WebAppConfiguration
@EnableWebMvc
public class SchedulerAndControllerTest {
	
	private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;
    
    @Autowired  
    private Scheduler scheduler;  
    
    @Autowired 
    private RestTemplate restTemplate;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
    @Test  
    public void shouldExecuteEpant_get_newsJob() throws Exception {  
    	scheduler.triggerJob(new JobKey("epant_get_news"));
    	Thread.sleep(10000);
    }
    
    @Test
    public void getAllPressReleasesAndCompareToOriginal() throws Exception {
    	
    	scheduler.triggerJob(new JobKey("epant_get_news"));
    	
    	String pressReleases = restTemplate.getForObject("https://www.epant.gr/openData.svc/GetNews", String.class);
    	
    	mockMvc.perform(get("/GetMyNews"))
        	.andExpect(status().isOk())
        	.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        	.andExpect(content().json(pressReleases));   	
    	
    }

}
